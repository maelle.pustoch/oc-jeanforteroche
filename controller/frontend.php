<?php

session_start();
ob_start();

// Chargement des classes
require_once('model/PostManager.php');
require_once('model/CommentManager.php');
require_once(__DIR__.'/../config.php');


function blogHome()
{
  $postManager = new \OpenClassrooms\Blog\Model\PostManager();
  $commentManager = new \OpenClassrooms\Blog\Model\CommentManager();
  $posts = $postManager->getPosts();

  foreach ($posts as $i => $post) {
    $comments = $commentManager->getComments($post['id'])->fetchAll();
    $totalComments = count($comments);
    $posts[$i]['total_comments'] = $totalComments; 
  }

  require('view/frontend/blog_home.php');
}

function blogArticle() {
  $postManager = new \OpenClassrooms\Blog\Model\PostManager();
  $commentManager = new \OpenClassrooms\Blog\Model\CommentManager();
  $post = $postManager->getPost($_GET['id']);
  if(!$post) {
    header('Location: /index.php?action=not_exists');
    exit;
  }

  $comments = $commentManager->getComments($_GET['id']);
  require('view/frontend/blog_article.php');
}


function notExists() {
  require('view/frontend/not_exists.php');
}


function addComment($postId, $author, $comment)
{
  $commentManager = new \OpenClassrooms\Blog\Model\CommentManager();
  $affectedLines = $commentManager->postComment($postId, $author, $comment);
  if ($affectedLines === false) {
    throw new Exception('Impossible d\'ajouter le commentaire !');
  } else {
    header('Location: /index.php?action=blog_article&id=' . $postId);
    exit;
  }
}


function commentsManagement() {
  $commentManager = new \OpenClassrooms\Blog\Model\CommentManager();
  $postManager = new \OpenClassrooms\Blog\Model\PostManager();
  $allPosts = $postManager->getPosts(); 
  $allComments = array(); 
  foreach ($allPosts as $eachPost) { 
    $comments = $commentManager->getComments($eachPost['id']); 

    foreach ($comments->fetchAll() as $comment) {
      $comment['post_title'] = $eachPost['title'];
      $allComments[] = $comment; 
    }  
  }
    require('view/frontend/comments_management.php');
}

function postManagement() {

  $postManager = new \OpenClassrooms\Blog\Model\PostManager();
  $commentManager = new \OpenClassrooms\Blog\Model\CommentManager();
  $posts = $postManager->getPosts();
  
  foreach ($posts as $i => $post) {
    $comments = $commentManager->getComments($post['id'])->fetchAll();
    $totalComments = count($comments);
    $posts[$i]['total_comments'] = $totalComments; 
  }

  require('view/frontend/posts_management.php');
}

function createPost()
{ 
  $postManager = new \OpenClassrooms\Blog\Model\PostManager();  
  
  if(isset($_POST['submit_post'])) {
    if(!empty($_POST['article'])) {
      $postManager->title = $_POST['title'];
      $postManager->content = $_POST['article'];
      $postManager->picture = $_POST['picture'];
      $post = $postManager->create();
      if($post === true) {
        $_SESSION['articlecreated'] = 'Votre article a bien été crée !';
      }
    } else {
      $_SESSION['contentmissing'] = 'Votre article ne peut pas être publié car il faut un contenu !';
    }
  }
  require('view/frontend/create_post.php');
}

function editPost()
{
  $postManager = new \OpenClassrooms\Blog\Model\PostManager();
  $post = $postManager->getPost($_GET['id']);
  
  if(isset($_POST['title']) && isset($_POST['article'])) {
    if(!empty($_POST['article'])) {
      $postManager->id = $_GET['id'];
      $postManager->title = $_POST['title'];
      $postManager->content = $_POST['article'];
      $postManager->picture = $_POST['picture'];
      $post = $postManager->update();
      $_SESSION['edited'] = "Vos modifications ont bien été prises en compte";
      header('Location: index.php?action=edit_post&id='.$_GET['id']);
      exit;
    } else {
      $_SESSION['articlemissing']= "Votre article ne peut pas être publié car il faut un contenu !";
    }
  } 
  require('view/frontend/edit_post.php');
}

function deletePost()
{  
  $postManager = new \OpenClassrooms\Blog\Model\PostManager();
  $commentManager = new \OpenClassrooms\Blog\Model\CommentManager();
  $post = $postManager->getPost($_GET['id']);
  $allComments = array();
  $allComments = $commentManager->getComments($post['id'])->fetchAll();
  foreach($allComments as $eachComment) {
     $commentManager->delete($eachComment['id']);
  }

  $postManager->delete($_GET['id']);
  $_SESSION['articledeleted'] = "Votre article a bien été supprimé";
  header("Location: /index.php?action=posts_management");
  exit;
}

function deleteComment()
{  
  $commentManager = new \OpenClassrooms\Blog\Model\CommentManager();
  $commentManager->delete($_GET['id']);
  $_SESSION['commentdeleted'] = "Votre commentaire a bien été supprimé";
  header('Location: /index.php?action=comments_management');
}

function login()
{ 
  global $username, $password;
    if(isset($_SESSION['admin'])) {
      header('Location:/index.php?action=posts_management');
      exit;
    }

    if (isset($_POST['submit_id'])) { 
      if (isset($_POST['mot_de_passe']) 
      AND $_POST['mot_de_passe'] == $password 
      AND isset($_POST['pseudo'])
      AND $_POST['pseudo'] == $username) {  
        $_SESSION['admin'] = 1; 

        header('Location:/index.php?action=posts_management');
        exit;
      } else { 
        header('Location:/index.php?action=login_page');
        exit;
      }
    }
  require('view/frontend/login_page.php');
}


function reportComment() {
  $commentManager = new \OpenClassrooms\Blog\Model\CommentManager();
  $postManager = new \OpenClassrooms\Blog\Model\PostManager();
  
  if($commentManager->reportComment($_GET['id'])) {
    $_SESSION['commentreported'] = "Votre commentaire a bien été signalé"; 
    header('Location: /index.php?action=blog_article&id=' . $_GET['post_id']);
    exit;
  }
}

function logOut() {
  session_destroy();
  header("Location: /index.php?action=blog_home");
 
 
}

function isLoggedIn() {
  if(!isset($_SESSION['admin']))  { 
    header('Location:/index.php?action=login_page');
    exit;
  } 





}















