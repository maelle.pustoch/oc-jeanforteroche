<?php
require('controller/frontend.php');

try {
    if (isset($_GET['action'])) {
        if ($_GET['action'] == 'blog_home') {
            blogHome();
        }
        elseif ($_GET['action'] == 'blog_article') {
            if (isset($_GET['id']) && $_GET['id'] > 0) {
                blogArticle();
            }
            else {
                throw new Exception('Aucun identifiant de billet envoyé');
            }
        }
        elseif ($_GET['action'] == 'addComment') {
            if (isset($_GET['id']) && $_GET['id'] > 0) {
                if (!empty($_POST['author']) && !empty($_POST['comment'])) {
                    addComment($_GET['id'], $_POST['author'], $_POST['comment']);
                }
                else {
                    throw new Exception('Tous les champs ne sont pas remplis !');
                }
            }
            else {
                throw new Exception('Aucun identifiant de billet envoyé');
            }
        }

        elseif($_GET['action'] == 'posts_management') {
          postManagement();
        }

        elseif($_GET['action'] == 'create_post') {
          createPost();
        } 

        elseif ($_GET['action'] == 'comments_management') {
         commentsManagement();            
        } 

        elseif ($_GET['action'] == 'edit_post') { 
          editPost();  
        }

        elseif ($_GET['action'] == 'login_page') {
          login();
        }

        elseif ($_GET['action'] == 'logOut') {
          logOut();
        }


        elseif($_GET['action'] == 'deleteComment') {
          deleteComment();
        }

        elseif($_GET['action'] == 'deletePost') {
          deletePost();
        }

        elseif($_GET['action'] == 'reportComment') {
          reportComment();
        }

        elseif($_GET['action'] == 'not_exists') {
          notExists();
        }

    }
        

    else {
        blogHome();
    }
    
}

catch(Exception $e) {

    echo 'Erreur : ' . $e->getMessage();
}
