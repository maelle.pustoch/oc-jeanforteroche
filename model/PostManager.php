<?php

namespace OpenClassrooms\Blog\Model;

require_once("model/Manager.php");

class PostManager extends Manager
{
  public $id;
  public $author = 'Jean Forteroche';
  public $title;
  public $content;
  public $creation_date;
  public $picture;


  public function create()
  {  
    $db = $this->dbConnect();
    $rq = $db->prepare('INSERT INTO posts (title, content, creation_date, picture) VALUES (?, ?, NOW(), ?)');
    $exe = $rq->execute(array($this->title, $this->content, $this->picture));
    // if (!$exe){ 
    //     var_dump($db->errorInfo());
    //   return $exe;
    // }

    return $exe;
  }

  public function update() 
  {
    $db = $this->dbConnect();
    $update = $db->prepare('UPDATE posts SET title = ?, content = ?, picture = ? WHERE id = ?');
    $updatePost = $update->execute(array($this->title, $this->content, $this->picture, $this->id));
    return $updatePost;
  }


  public function getPosts()
  {
    $db = $this->dbConnect();
    $req = $db->query('SELECT id, title, content, picture,
      DATE_FORMAT(creation_date, \'%d/%m/%Y à %Hh %imin\') AS creation_date_fr FROM posts ORDER BY creation_date DESC');
    $req->execute();
    return $req->fetchAll();
 
  }

  public function getPost($postId) {
    $db = $this->dbConnect();
    $req = $db->prepare('SELECT id, title, content, picture, DATE_FORMAT(creation_date, \'%d/%m/%Y à %Hh%imin%ss\') AS creation_date FROM posts WHERE id = ? ');
    $req->execute(array($postId)); //array  quand ? dans requete
    $post = $req->fetch();

    return $post;
  }

  public function delete($postId) 
  {
    $db = $this->dbConnect();
    $delete = $db->prepare('DELETE FROM posts WHERE id = ?');
    $delete->execute(array($postId));
  }
}
?>