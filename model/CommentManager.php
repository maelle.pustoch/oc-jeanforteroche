<?php

namespace OpenClassrooms\Blog\Model;

require_once("model/Manager.php");

class CommentManager extends Manager
{
    public function getComments($postId)
    {
      $db = $this->dbConnect();
      $comments = $db->prepare('SELECT id, post_id, author, comment, flag, DATE_FORMAT(comment_date, \'%d/%m/%Y à %Hh %imin %ss\') AS comment_date_fr FROM comments WHERE post_id = ? ');
      $comments->execute(array($postId));

      return $comments; 
    }

 
    public function postComment($postId, $author, $comment)
    {
      $db = $this->dbConnect();
      $comments = $db->prepare('INSERT INTO comments (post_id, author, comment, comment_date) VALUES(?, ?, ?, NOW())');
      $affectedLines = $comments->execute(array($postId, $author, $comment));

      if (!$affectedLines) { 
          echo "erreur db";
          print_r($db->errorInfo());
      }
       return $affectedLines;
    }


    public function delete($commentId)
    {
      $db = $this->dbConnect();
      $delete_com = $db->prepare('DELETE FROM comments WHERE id = ?');
      $delete_com->execute(array($commentId));
    }



    public function reportComment($commentId) 
    {
      $db = $this->dbConnect();
      $report = $db->prepare('UPDATE comments SET flag = 1 WHERE id = ? ORDER BY flag DESC');
      $returnComment = $report->execute(array($commentId));
      return $returnComment;

    }




}
