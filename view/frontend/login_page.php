<?php ob_start(); ?>
<?php $title = 'Mon blog'; ?>

<div class = "container-fluid" id ="signin">
  <div class = "row mt-5 justify-content-center password text-center">
    <img src="./public/images/author-article.jpg" alt=" article-author-picture" class="rounded-circle mt-5 mb-5" style ="height: 200px; width: 220px; ">
  </div>
  
  <div class = "row">
    <div class = "col welcome text-center mb-5">
      <h1> Bonjour Jean !</h1>
    </div>
  </div>

  <div class = "row">
    <div class = "col text-center">
      <form action="/index.php?action=login_page" method="POST">
        <input type="text" name="pseudo" class= "bg-transparent" placeholder = "Pseudo" style = "outline: 0; border-width: 0 0 1px; border-color: grey"required/>
        <br>
        <input type="password" name="mot_de_passe" class="mt-4" placeholder="mot de passe" style = "outline: 0; border-width: 0 0 1px; border-color: grey"  required>
        <br>
        <a class="btn btn-outline-secondary mt-5 px-3 mr-2" href = "index.php">retour</a>
       <button class="submit_id btn mt-5 btn-info px-3" type="submit" name="submit_id">Se connecter</button>
      </form>
    </div>
  </div>
</div>



<?php $content = ob_get_clean(); ?>
<?php require('template.php'); ?>



