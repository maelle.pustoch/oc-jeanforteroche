<?php isLoggedIn(); ?>
<?php $title = 'Mon blog'; ?>
<?php ob_start(); ?>

<?php include("menu_responsive.php"); ?> 
<?php include('topbar_menu.php') ?>


<div class = "container-fluid p-0">
	<div class = "d-flex d-row">
		<div class = "nav sidenav">
			<?php include("backofficemenu.php"); ?>
		</div>

		<div class = "section table-responsive table mt-5 bg-transparent justify-content-center">
			<div class = "row justify-content-left d-flex mr-3">
				<div class = "col welcome">
					<h2 class = "ml-5 mb-5"> Bienvenue Jean !</h2>
				</div>
				<div class = "notifs">
					<?php if(isset($_SESSION['articledeleted'])) { ?>
						<div class="d-flex justify-content-end alert alert-danger" role="alert">
							<?php echo $_SESSION['articledeleted'];
							unset($_SESSION['articledeleted']);?>
						</div>
					<?php } ?>
				</div>
			</div>
			<table class= "table table-striped table-hover text-center">
				<thead>
				  <tr>
				    <th scope="col">ID</th>
				    <th scope="col">Titre</th>
				    <th scope="col">Nombre de commentaires</th>
				    <th scope="col">Date de derniere mise à jour</th>
				    <th scope="col">Modération</th>
				  </tr>				 
				</thead>		
				<tbody>
					<?php foreach ($posts as $post) { ?>
						<tr>
							<td>
								<?php echo $post['id'];  ?>
							</td>
							<td>
								<?php echo $post['title']; ?>
							</td>
							<td>
								<?php echo $post['total_comments']; ?>
							</td>
							<td>
								<?php echo $post['creation_date_fr'];  ?>
							</td>
					   	<td>
					   		<div class="buttons d-flex">
					 				<a class=" d-inline btn btn-white mr-2" href= '/index.php?action=blog_article&id=<?= $post['id'] ?>'><i class= " text-success fas fa-desktop"></i></a>			 				
					 				<a class="d-inline btn btn-white mr-2" href = '/index.php?action=edit_post&id=<?= $post['id'] ?>'><i class="px-auto text-warning far fa-edit"></i></a>
									<a class="d-inline btn btn-white mr-2" href = '/index.php?action=deletePost&id=<?= $post['id'] ?>'><i class=" text-danger far fa-trash-alt"></i></a>	
								</div>					 	
							</td>
						</tr> 
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>



<?php $content = ob_get_clean(); ?>
<?php require('template.php'); ?>

