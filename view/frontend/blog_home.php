<?php $title = 'Jean Forteroche'; ?>
<?php ob_start(); ?>

<div class="container-fluid">
  <div class ="header row">
    <?php include("menu.php"); ?>
    <div class = "col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 img-cover position-relative col" id="cover">
      <div class ="img-mountain img-fluid"></div>

      <div class = "row mt-2">
        <div class = "title col-xs-8 col-sm-6 col-md-5 col-lg-4 offset-md-1 text-xs-center text-white position-absolute">
          <h1>Un Billet Simple pour l'Alaska</h1>
          <br>
          <h6 class = "font-weight-light text-white" style = "font-family: 'Roboto thin', sans-serif;"> Plongez au coeur de l'Alaska  dans le nouveau roman de Jean Forteroche.</h6>
          <br>
          <button type="button" class="btn btn-outline-light px-5" id="cover-button">En savoir Plus</button>
        </div>
      </div>
    </div>
  </div>

  <div class = "article row mt-5" id= "last-posts">
    <div class = "left-side col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
      <h2 class = "text-center text-secondary">Les derniers Articles </h2>

      <br>
      <!-- <p class = "text-center"></p> -->
      <div class= "card-columns card-columns-0">
        <?php $slice_posts = array_slice($posts, 0, 6);
          foreach ($slice_posts as $data)
          { ?>  
        
        <div class="card" style="max-width: 18rem;">
          <img class="card-img-top u-pointer" src="           
         <?php if(empty($data['picture'])) { ?> 
              <?= 'https://images.unsplash.com/photo-1479981280584-037818c1297d?ixlib=rb-1.2.1&auto=format&fit=crop&w=2250&q=80'?> 
          <?php } else { ?> 
            <?= $data['picture']?> 
          <?php } ?> )" alt="Card image cap" >
          <div class="card-body">
            <h4 class="card-title">
              <a style = "color:grey;" href = "/index.php?action=blog_article&id=<?= $data['id'] ?>"><?= htmlspecialchars_decode($data['title']);?></a>  
            </h4>
        
              <p class="card-text">
                <?= htmlspecialchars_decode(substr($data['content'],0,120)); ?></p> 
              <p style = "font-size:0.8em; color:#72aeb5;"> Publié le <?php echo $data['creation_date_fr'];  ?> 
              <br>
              <i class="fas fa-comment"></i> <?php echo $data['total_comments']; ?> </p>
          </div>
        </div>
        <?php }  ?> 
      </div>
    </div>

 

        
    <div class = "aside right-side col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6" id = "author-side">
      <div class = "img-aube">
        <div class = "green-filter"></div>
      </div>
      <div class = "author-half"></div>
      <div class = "text-intro">
        <h2>Jean Forteroche </h2>
        <!-- <div class = "row"> -->
        <p class = "col-10 "> Ut enim quisque sibi plurimum confidit et ut quisque maxime virtute et sapientia sic munitus est, ut nullo egeat suaque omnia in se ipso posita iudicet, ita in amicitiis expetendis colendisque maxime excellit. Quid enim? Africanus indigens mei? Minime hercule! ac ne ego quidem illius; sed ego admiratione quadam virtutis eius, ille vicissim opinione fortasse non nulla, quam de meis moribus habebat, me dilexit; auxit benevolentiam consuetudo. Sed quamquam utilitates multae et magnae consecutae sunt, non sunt tamen ab earum spe causae diligendi profectae.
        <br><br>
        Ut enim quisque sibi plurimum confidit et ut quisque maxime virtute et sapientia sic munitus est, ut nullo egeat suaque omnia in se ipso posita iudicet, ita in amicitiis expetendis colendisque maxime excellit. Quid enim? Africanus indigens mei? 
        <br>
        </p>
        <button type = button class=" btn btn-light px-5 mt-4 " id ="purchase"><a href = "https://www.fnac.com/" target="_blank" class = "text-decoration-none text-secondary">Commander</a></button>
        <!-- </div> -->
      </div>
    </div>
  </div>

  <div class="section jumbotron jumbotron-fluid mt-3">
    <blockquote class="blockquote text-center my-5">
      <h5 class="mb-0">Un livre envoûtant et hâletant, une vrai réussite. Ne passez pas à côté !</h5>
      <footer class="blockquote-footer"> Raphaël Moté <cite title="Source Title">Le Monde</cite><br>
        <i class="d-inline fas fa-star"></i>
        <i class="d-inline fas fa-star"></i>
        <i class="d-inline fas fa-star"></i>
        <i class="d-inline fas fa-star"></i>
        <i class="d-inline fas fa-star"></i></footer>
    </blockquote>
    <hr class = "col-2 mx-auto ">
        <blockquote class="blockquote text-center my-5">
      <h5 class="mb-0">Vivez et Ressenter l'Alaska sans bouger de votre fauteuil !</h5>
      <footer class="blockquote-footer">Catherine Langevin <cite title="Source Title">Les Inrocks</cite><br>
        <i class="d-inline fas fa-star"></i>
        <i class="d-inline fas fa-star"></i>
        <i class="d-inline fas fa-star"></i>
        <i class="d-inline fas fa-star"></i>
        </footer>
    </blockquote>
  </div>

<!-- <div class="jumbotron mt-5">

</div> -->


  <div class = "article row last-articles justify-content-center mb-5 mt-5" id= "all-articles">
    <div class = "col-12 text-center ">
      <h2 class = "text-center text-secondary">Tous les articles </h2>
      <p class = "justify-content-center col-xs-10 col-sm-10 col-md-6 col-lg-6 col-xl-6 mx-auto my-5"> Ut enim quisque sibi plurimum confidit et ut quisque maxime virtute et sapientia sic munitus est, ut nullo egeat suaque omnia in se ipso posita iudicet, ita in amicitiis expetendis colendisque maxime excellit. Quid enim? Africanus indigens mei? Ut enim quisque sibi plurimum confidit et ut quisque maxime virtute et sapientia sic munitus est, ut nullo egeat suaque omnia in se ipso posita iudicet, ita in amicitiis expetendis colendisque maxime excellit. Quid enim? Africanus indigens mei?</p>
      <hr class ="col-4 mx-auto my-5">

    </div>
    <div class = "row justify-content-center mb-5 mr-0" id= "blog">
      <div class = "col-lg-11 col-md-11 col-xs-11 col-xl-11 col-sm-11 cards ">
        <?php include("cards.php"); ?>
      </div>
    </div>
    </div>
  </div>
</div>






<?php include("footer.php") ?>
<?php $content = ob_get_clean(); ?>
<?php require('template.php'); ?>


