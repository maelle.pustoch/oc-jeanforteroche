	<!-- Footer -->
<footer class="page-footer font-small mdb-color lighten-3 pt-4 text-white mt-5" style = "background-color: #c6cbcc;" id ="footer">

  <!-- Footer Links -->
  <div class="container text-center text-md-left">

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-4 col-lg-3 mr-auto my-md-4 my-0 mt-4 mb-1">

        <!-- Content -->
        <h3 class="font-weight-bold mb-4">Jean Forteroche</h3>
        <p>Auteur, voyageur Jean Forteroche partage à travers ce blog le récit de son dernier voyage en Alaska qui l'inspira son dernier récit "Un Billet simple pour l'Alaska".</p>
        <?php if(!isset($_SESSION['admin'])) { ?> 
        <a class=" mx-auto btn btn btn-outline-light btn-login rounded-pill px-4 " id="cover-button" href="index.php?action=login_page" "><i class="fas fa-user pr-3" ></i>Connexion</a>
        <?php } ?>
      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none">

      <!-- Grid column -->
<!--  -->
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none">

      <!-- Grid column -->
      <div class="col-md-4 col-lg-3 mx-auto my-md-4 my-0 mt-4 mb-1">

        <!-- Contact details -->
        <h5 class="font-weight-bold text-uppercase mb-4">Address</h5>

        <ul class="list-unstyled">
          <li>
            <p>
              <i class="fas fa-home mr-3"></i> Paris, France</p>
          </li>
          <li>
            <p>
              <i class="fas fa-envelope mr-3" href= "mailto:JeanForteroche@yopmail.com"></i> JeanForteroche@yopmail.com</p>
          </li>
          <li>
            <p>
              <i class="fas fa-phone mr-3"></i> + 331 234 567 88</p>
          </li>
          <li>
            <p>
              <i class="fas fa-print mr-3"></i> + 331 234 567 89</p>
          </li>
        </ul>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none">

      <!-- Grid column -->
      <div class="col-md-2 col-lg-2 text-center mx-auto my-4">

        <!-- Social buttons -->
        <h5 class="font-weight-bold text-uppercase mb-4">Follow Us</h5>

        <!-- Facebook -->
        
          <i class="fab fa-facebook-square"></i>
       
        <!-- Twitter -->
      
           <i class="fab fa-twitter pl-1" ></i>
      
        <!-- Google +-->
       
           <i class="fab fa-linkedin-in pl-1"></i>
        
        <!-- Dribbble -->
       
          <i class="fab fa-dribbble"></i>

          <i class="fab fa-behance"></i>
        </a>
      </div>
    </div>
  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3 " style = "background-color: #bcc1c2;"><p> 2019 Copyright: Maëlle Pustoc'h - Projet Pédagogique </a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
	



