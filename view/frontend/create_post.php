<?php isLoggedIn(); ?>
<?php $title = 'Back Office'; ?>
<?php ob_start(); ?>


<script>
		tinymce.init({
		  selector: 'textarea#basic-example',
		  height: 400,
		  menubar: 'file edit insert view format table tools help',
		  plugins: [
		    'advlist autolink lists link image charmap print preview anchor textcolor',
		    // 'searchreplace visualblocks code fullscreen', 
		    'insertdatetime media table paste code help wordcount'
		  ],
		  toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
		  content_css: [
		    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
		    '//www.tiny.cloud/css/codepen.min.css'
		  ] 
		});
</script>	

<?php include("menu_responsive.php"); ?> 
<?php include('topbar_menu.php') ?>

<div class="container-fluid p-0">
	<div class="d-flex d-row">
		<div class="nav sidenav">
			<?php include("backofficemenu.php"); ?>
		</div>
		<div class = "section table-responsive table mt-5 bg-transparent justify-content-center">
			<div class = "d-row d-flex justify-content-left mr-3">
				<div class = "col publish">
					<h2 class = "ml-1 mb-3">Publier un Article</h2>
				</div>
				<div class = "notifs d-flex justify-content-end">
					<?php if(isset($_SESSION['articlecreated'])) {?>
						<div class="alert alert-success" role="alert">
							<?php 
						  	echo $_SESSION['articlecreated']; 
						  	unset($_SESSION['articlecreated']); 
						  ?> <br>
							<a href="/index.php" class="alert-link">Voir le Blog</a>. 
						</div>
					<?php } ?>

					<?php if(isset($_SESSION['contentmissing'])) { ?>
						<div class="alert alert-danger" role="alert">
					  <?php 
					  	echo $_SESSION['contentmissing']; 
					  	unset($_SESSION['contentmissing']);?>  
						</div>
					<?php } ?>
				</div>
			</div>
			<div class="section col-11">
				<form action ="/index.php?action=create_post" method="POST">
			    <input class="title-post p-0 mt-1 mb-4 col-12" name="title" type="title" placeholder="Titre" required><br>
			    <input class= "p-0 mt-1 mb-4 col-12" name="picture" type="title" placeholder="URL de l'image">
					<textarea id="basic-example" name="article" class = "p-0 col-12"></textarea>
					<input class="btn btn-success mt-4 px-4" type="submit" name= "submit_post" value="Publier" />
				</form>
			</div>
		</div>
	</div>
</div>



<?php $content = ob_get_clean();?>
<?php require('template.php'); ?>


