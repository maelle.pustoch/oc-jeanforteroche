<!-- Menu  -->
<nav class="navbar navbar-expand-lg navbar-light col-12 fixed-top">
  <!-- <a class="navbar-brand" href="#"></a> -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto" style = "font-family: 'DM Serif Display', serif;">
      <li class="nav-item active">
        <a class="nav-link text-uppercase text-dark u-pointer" id = "homelink" href = "index.php#cover">Home </a>
      </li>
      <li class="nav-item active">
        <a class="nav-link text-uppercase text-dark u-pointer" id = "bloglink" href="index.php#blog">Blog</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link text-uppercase text-dark u-pointer" id = "contactlink" href="#footer">Contact</a>
      </li>
    </ul>


  <?php if(isset($_SESSION['admin'])) { ?> 

      <a class="btn btn-info border-none mr-4" href="/index.php?action=create_post">
        <i class="fas fa-feather"></i>
      </a> 

      <a class="btn btn-light px-4" href="/index.php?action=posts_management">Admin</a>  
      
      <!-- <a class="nav-link" href="index.php?action=logOut"> -->
      <a class="btn border-none" href="index.php?action=logOut">
        Deconnexion <i class="ml-2 text-muted fas fa-times "></i>
      </a> 
 
  <?php }?> 
    


  </div>
</nav>