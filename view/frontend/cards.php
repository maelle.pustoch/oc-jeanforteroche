
  <div class="card-columns" id = "card-columns">
    <?php $slice_posts = array_slice($posts,0,100);
    foreach ($slice_posts as $post)
    { ?>  
      <div class="card" style="max-width: 18rem;">
        <img class="card-img-top" src="           
         <?php if(empty($post['picture'])) { ?> 
              <?= 'https://images.unsplash.com/photo-1479981280584-037818c1297d?ixlib=rb-1.2.1&auto=format&fit=crop&w=2250&q=80'?> 
          <?php } else { ?> 
            <?= $post['picture']?> 
          <?php } ?> )" alt="Card image cap">
        <div class="card-body">
          <h4 class="card-title">
            <a style = "color:grey;" href = "/index.php?action=blog_article&id=<?= $post['id']; ?>"><?= htmlspecialchars_decode($post['title']);?></a></h4>
          <p class="card-text">
            <?= htmlspecialchars_decode(substr($post['content'],0,120)); ?></p>  
          <p style = "font-size:0.8em; color:#72aeb5;">
          Publié le <?php echo $post['creation_date_fr'];?>
          <br>
          <i class="fas fa-comment"></i> <?php echo $post['total_comments']; ?></p>
        </div>
      </div>
    <?php } ?>
  </div> 



 