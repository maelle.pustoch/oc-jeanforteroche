<?php isLoggedIn(); ?>
<?php $title = 'Mon blog'; ?>
<?php ob_start(); ?>
<?php include("menu_responsive.php"); ?> 
<?php include('topbar_menu.php') ?>

<div class="container-fluid p-0">
	<div class="d-flex d-row">
		<div class="nav sidenav">
			<?php include("backofficemenu.php"); ?>
		</div>
		<div class = "section table-responsive table mt-5 bg-transparent justify-content-center">
			<div class = "row justify-content-start d-flex mr-3">
				<div class = "col comments-management">
					<h2 class = "ml-5 mb-5">Gérer les commentaires</h2>
				</div>
				<div class = "notifs">
					<?php if(isset($_SESSION['commentdeleted'])) { ?>
						<div class="d-flex justify-content-end alert alert-danger" role="alert">
							<?php echo $_SESSION['commentdeleted'];
							unset($_SESSION['commentdeleted']);?> 
						</div>
					<?php } ?>
				</div>
			</div>
			<table class="table table-striped table-hover text-center">
				<thead>
				  <tr>
				    <th scope="col">ID</th>
				    <th scope="col">Titre de l'Article</th>
				    <th scope="col">Auteur</th>
				    <th scope="col">Dernier Commentaire ajouté</th>
				    <th scope="col">Date du commentaire</th>
				    <th scope='col'>Signalé</th>
				    <th scope="col">Modération</th>
				  </tr>				 
				</thead>		
				<tbody>
					<?php foreach ($allComments as $comment) { ?>
						<tr>
							<td>
								<?php echo $comment['post_id']?>
							</td>
							<td>
								<?php echo $comment['post_title']?>
							</td>
							<td>
								<?php echo $comment['author']?>
							</td>
							<td>
								<?php echo $comment['comment']?>
							</td>
							<td>
								<?php echo $comment['comment_date_fr']?>
							</td>
							<td>
								<?php echo $comment['flag']?>
							</td>
							<td>
							<div class = "d-flex d-row">
				 				<a href = '/index.php?action=deleteComment&id=<?=$comment['id']?>' class="btn btn-white "><i class=" text-danger far fa-trash-alt"></i></a>
				 			</div>
							</td>
						</tr>	
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?php $content = ob_get_clean(); ?>
<?php require('template.php'); ?>



