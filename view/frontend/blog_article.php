
<?php $title = htmlspecialchars($post['title']); ?>
<?php ob_start(); ?>

<?php include("menu.php"); ?>

<!-- <p><a href="index.php">Retour à la liste des billets</a></p> -->

<div class = "container-fluid">
  <div class="headder news">
    <div class = "row">
      <div class = "col">
        <div class="header-img col-12 p-0 mt-3 m-0" >
          <div class="img-article position-relative" style = "background-image : url(
            <?php if(empty($post['picture'])) { ?> 
              <?= 'https://images.unsplash.com/photo-1479981280584-037818c1297d?ixlib=rb-1.2.1&auto=format&fit=crop&w=2250&q=80'?> 
          <?php } else { ?> 
            <?= $post['picture']?> 
          <?php } ?> )">

            <div class="img-title position-absolute mt-5 pt-5 col-xs-3 offset-xs-3 col-sm-7 offset-sm-3 offset-md-3 text-center font-weight-bold text-white">
              <h5 style = "font-size:2rem;"><?= htmlspecialchars($post['title']) ?>
              <!--  -->
              </h5>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class = "aside container">
      <div class = "row">
        <div class = "flex-xs-column d-sm-flex flex-sm-column d-md-flex flex-md-row offset-md-1 col-md-10 col-lg-6 flex-lg-row text-justify text-center">
          <div class ="edition-info d-flex flex-column col-sm-12 col-md-3 col-lg-5 mt-4 text-md-right text-lg-right">
            <div class = "offset-xs-6 offset-lg-2">
              <img src="./public/images/author-article.jpg" alt=" article-author-picture" class="rounded-circle rounded-circle shadow-sm author-img">
            </div>
            <div class = "author-name text-secondary text-xs-center text-lg-right text-md-right mt-md-4 mt-4">
                <h5> Jean Forteroche </h5> 
            </div>
            <div class = "article-date text-xs-center text-secondary">
            </div>
            <a class="btn btn-outline-info u-pointer mt-4 text-lowercase" href = "index.php"> retour </a>
          </div>

          <div class = "article article-content col-xs-8 col-sm-11 col-md-9 offset-md-11 col-lg-11 offset-lg-1 mt-4 align-items-xs-center align-items-sm-center text-justify">
            <p>
              <?= nl2br(htmlspecialchars_decode($post['content'])) ?>
            </p>
          </div>
        </div>
      </div>
    
      <div class = "section row mt-5 justify-content-center text-center"> 
        <div class = "share-options font-weight-bold ">
            <p>Partager cet Article </p>
          <div class ="social text-center">
            <i class="fab fa-facebook-square"></i>
            <i class="fab fa-twitter pl-1" ></i>
            <i class="fab fa-linkedin-in pl-1"></i>
          </div>
          <div class ="edition-info col author-name text-secondary text-center mt-4 ">
            <h5> Jean Forteroche </h5>
            <img src="./public/images/author-article.jpg" alt=" article-author-picture" class="rounded-circle mt-4 author-img-2">
          </div>
          <br>
          <hr>
        </div>
      </div>
    </div>
      
    <div class = "section row justify-content-center text-center">
      <div class ="comments-section my-5 text-center text-secondary justify-content-center">
        <h3 class = "my-4">Commentaires</h3>
        <form action="/index.php?action=addComment&amp;id=<?= $post['id'] ?>" method="POST" class =  "d-flex justify-content-center flex-column">
          <input type="text" id="author" name="author" class = "mt-2 rounded" placeholder="Votre Pseudo" required="" /><br>
          <textarea id="comment" name="comment" class = "mt-2 border-secondary" placeholder = "Votre commentaire" required></textarea>
          <input class = "btn btn-outline-info my-4 rounded-pill"  type="submit" value = "Valider"/>
        </form>
      </div>
    </div>

    <div class = "section row justify-content-center text-center">
      <div class = "col-xs-10 col-sm-6 col-md-6 col-lg-4 col-xl-4 mt-4 text-center justify-content-center">
      <?php while ($comment = $comments->fetch()) { ?>
        <p class = "d-inline"><strong><?= htmlspecialchars($comment['author']) ?></strong><br> 
        <i>le <?= $comment['comment_date_fr'] ?></i></p><br><br> 
        <p><?= nl2br(htmlspecialchars($comment['comment'])) ?></p>
        
        <div class = "text-center mt-4">
          <a class=" d-inline btn btn-outline-warning btn-sm px-4" href ='/index.php?action=reportComment&id=<?=$comment['id'] ?>&post_id=<?=$post['id'] ?>' name = 'report_comment'><i class="fas fa-flag"></i></a>
        <?php if(isset($_SESSION['commentreported'])) { ?>
          <div class="d-inline alert alert-warning" role="alert">
            <?php echo $_SESSION['commentreported'];
            unset($_SESSION['commentreported']);?>
          </div>
        <?php } ?>
        </div>
        <hr>
      <?php } ?>

 
    </div>
  </div>
</div>


      

<?php include("footer.php") ?>
<?php $content = ob_get_clean(); ?>
<?php require('template.php'); ?>
