<?php isLoggedIn(); ?>
<?php $title = 'Mon blog'; ?>
<?php ob_start(); ?>
		
<script>
	tinymce.init({
	  selector: 'textarea#basic-example',
	  height: 400,
	  menubar: 'file edit insert view format table tools help',
	  plugins: [
	    'advlist autolink lists link image charmap print preview anchor textcolor',
	    // 'searchreplace visualblocks code fullscreen', 
	    'insertdatetime media table paste code help wordcount'
	  ],
	  toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
	  content_css: [
	    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
	    '//www.tiny.cloud/css/codepen.min.css'
	  ] 
	});
</script>
<?php include('topbar_menu.php') ?>
	
<div class="container-fluid p-0">
	<div class="d-flex d-row">

		<div class="nav sidenav">
			<?php include("backofficemenu.php"); ?>
		</div>
		<div class = "section table-responsive table mt-5 bg-transparent justify-content-center">
			<div class = "d-row d-flex justify-content-left mr-3">
				<div class = "col welcome">
					<h2 class = "ml-4 mb-3">Modifier un Article</h2>
				</div>
				<div class = "notifs">
					<?php if(isset($_SESSION['edited'])) { ?>
						<div class="d-flex justify-content-end alert alert-warning" role="alert">
							<?php echo $_SESSION['edited'];
							unset($_SESSION['edited']);?> 
						</div>
					<?php } ?>
					<?php if(isset($_SESSION['articlemissing'])) { ?>
						<div class="d-flex justify-content-end alert alert-danger" role="alert">
							<?php echo $_SESSION['articlemissing'];
							unset($_SESSION['articlemissing']);?>  
						</div>
					<?php } ?>
				</div>
			</div>
			<div class="section col-11">
				<form action="/index.php?action=edit_post&id=<?= $post['id']; ?>" method="post">
	    		<input class="title p-0 mt-1 mb-4 col-12" name="title" type="title" value="<?= $post['title']; ?>">
	    		<input class="title p-0 mt-1 mb-4 col-12" name="picture" type="picture" placeholder="Image d'en tête" value = "<?= $post['picture']; ?>" >
					<textarea id="basic-example" name="article" class ="p-0 md-offset-4"> 
						<?= $post['content']; ?>
					</textarea>
					<div class = "buttons mt-5">
						<a class="d-inline btn btn btn-outline-dark mr-2" href = '/index.php?action=edit_post&id=<?= $post['id'] ?>'>Annuler</a>
					 	<input class="d-inline btn btn-success px-4 justify-content-left" type="submit" name= "update_post" value="Publier" >
				 	</div> 
    		</form>
			</div>
		</div>
	</div>
</div>



<?php $content = ob_get_clean(); ?>
<?php require('template.php'); ?>