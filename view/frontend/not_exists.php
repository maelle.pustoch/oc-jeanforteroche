<?php $title = 'Mon blog'; ?>

<?php ob_start(); ?>

<div class="container-fluid">
	<div class = "row">
		<?php include("menu.php"); ?>
		
		<div class = "col-12 mb-5">
			<div class ="img-desert">
				<div class ="row">
					<div class = "col-6 text-center mt-5 offset-3" >
						<h2 class = "text-white" style = "margin-top:40%;"> Oups, il n'y a rien ici </h2>			
						<a class="btn btn-outline-light mt-4 u-pointer bg-transparent " href= "index.php">
							<i class="fas fa-th p-1" href= "index.php"></i>Retour au Blog</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<?php include("footer.php") ?>
<?php $content = ob_get_clean(); ?>
<?php require('template.php'); ?>